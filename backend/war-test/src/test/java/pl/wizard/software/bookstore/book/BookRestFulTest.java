package pl.wizard.software.bookstore.book;


import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.junit.jupiter.api.Test;


import java.io.FileReader;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class BookRestFulTest {

    @Test
    public void shouldReturnWholeBookWithId1() throws IOException {
        restTest("http://localhost:8080/war/book/1", "book/1.json");
    }

    @Test
    public void shouldReturnAllIBooks() throws IOException {
        restTest("http://localhost:8080/war/book", "book/all.json");
    }

    @Test
    public void shouldReturnAllIBooksWrittenByKentBeck() throws IOException {
        restTest("http://localhost:8080/war/book/writtenBy?authorName=Kent&authorSurname=Beck", "book/beck.json");
    }

    @Test
    public void shouldReturnAllBookWrittenBetween2005and2015() throws IOException {
        restTest("http://localhost:8080/war/book/writtenInYears?from=2005&to=2015", "book/between2005and2015.json");
    }

    private void restTest(String aRestUrl, String aPreparedFilePath) throws IOException {
        //given - prepare html request
        HttpGet getRequest = new HttpGet(aRestUrl);
        //given - prepare expected result
        JsonParser parser = new JsonParser();
        FileReader preparedFile = new FileReader(
                getClass().getClassLoader().getResource(aPreparedFilePath).getFile());
        JsonElement expected = parser.parse(preparedFile);

        //when make rest call
        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(getRequest);
        String content = EntityUtils.toString(httpResponse.getEntity());

        //then
        assertEquals(200, httpResponse.getStatusLine().getStatusCode());

        JsonElement result = parser.parse(content);
        assertEquals(expected, result);
    }
}