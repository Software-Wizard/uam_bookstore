package pl.wizard.software.bookstore.book;

import pl.wizard.software.entities.AuthorEntity;
import pl.wizard.software.entities.BookEntity;

public class BookDtoToEntity {

    public static BookEntity toEntity(BookDto aEnt){
        BookEntity ret = new BookEntity(aEnt.getId());
        String[] split = aEnt.getAuthor().split(" ");
        ret.addAuthor(new AuthorEntity(split[0],split[1]));
        ret.setPrice(aEnt.getPrice());
        ret.setRating(aEnt.getRating());
        ret.setReleaseDate(aEnt.getReleaseDate());
        ret.setTitle(aEnt.getTitle());
        return ret;
    }
}
