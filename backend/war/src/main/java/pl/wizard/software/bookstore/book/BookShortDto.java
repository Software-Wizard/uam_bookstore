package pl.wizard.software.bookstore.book;

import pl.wizard.software.entities.BookEntity;

public class BookShortDto {

    private final Long id;
    private final String author;
    private final String title;

    public BookShortDto(Long id, String author, String title) {
        this.id = id;
        this.author = author;
        this.title = title;
    }

    public BookShortDto(BookDto aBookDto){
        id = aBookDto.getId();
        author = aBookDto.getAuthor();
        title = aBookDto.getTitle();
    }

    public BookShortDto(BookEntity aEntity) {
        id = aEntity.getId();
        author = aEntity.getAuthor().getName() + " " + aEntity.getAuthor().getSurname();
        title = aEntity.getTitle();
    }

    public Long getId() {
        return id;
    }

    public String getAuthor() {
        return author;
    }

    public String getTitle() {
        return title;
    }
}
