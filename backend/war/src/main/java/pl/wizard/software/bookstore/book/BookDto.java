package pl.wizard.software.bookstore.book;

import pl.wizard.software.entities.BookEntity;

import java.io.Serializable;

public class BookDto implements Serializable {

    private Long id;
    private String author;
    private String title;
    private double price;
    private double rating;
    private int releaseDate;

    public BookDto() {

    }

    public BookDto(long aId) {
        id = aId;
    }

    public BookDto(Long id, String author, String title, double price, double rating, int releaseDate) {
        this.id = id;
        this.author = author;
        this.title = title;
        this.price = price;
        this.rating = rating;
        this.releaseDate = releaseDate;
    }

    public BookDto(BookEntity bookEntity) {
        this.id = bookEntity.getId();
        this.author = bookEntity.getAuthor().getName() + bookEntity.getAuthor().getSurname();
        this.title = bookEntity.getTitle();
        this.price = bookEntity.getPrice();
        this.rating = bookEntity.getRating();
        this.releaseDate = bookEntity.getReleaseDate();
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public String getAuthor() {
        return author;
    }

    public String getTitle() {
        return title;
    }

    public double getPrice() {
        return price;
    }

    public double getRating() {
        return rating;
    }

    public int getReleaseDate() {
        return releaseDate;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public void setReleaseDate(int releaseDate) {
        this.releaseDate = releaseDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BookDto bookDto = (BookDto) o;

        return id != null ? id.equals(bookDto.id) : bookDto.id == null;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
