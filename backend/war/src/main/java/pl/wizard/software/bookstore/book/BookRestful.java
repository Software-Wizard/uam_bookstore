package pl.wizard.software.bookstore.book;

import pl.wizard.software.BookDao;
import pl.wizard.software.entities.BookEntity;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.List;
import java.util.stream.Collectors;

@Path("book")
@RequestScoped
public class BookRestful {


    @EJB
    private BookDao bookDao;

    @GET
    @Path("{id}")
    @Produces("application/json; charset=UTF-8")
    public Response getById(@PathParam("id") long aId) {
        BookDto ret = new BookDto(bookDao.getBookById(aId).get());
        return Response.status(200).entity(ret).build();
    }

    @GET
    @Produces("application/json; charset=UTF-8")
    public Response getAll() {
        List<BookShortDto> ret = bookDao.getAll().stream().map(BookShortDto::new).collect(Collectors.toList());
        return Response.status(200).entity(ret).build();
    }

    @GET
    @Path("test")
    @Produces("application/json; charset=UTF-8")
    public void test() {
        bookDao.test();
    }


    @GET
    @Path("writtenBy")
    @Produces("application/json; charset=UTF-8")
    public Response getBooksWrittenBy(@Context UriInfo info) {
        String name = info.getQueryParameters().getFirst("authorName");
        String surname = info.getQueryParameters().getFirst("authorSurname");

        List<BookShortDto> ret = bookDao.getAll().stream().filter(book -> book.getAuthor().equals(name + " " + surname)).map(BookShortDto::new).collect(Collectors.toList());
        return Response.status(200).entity(ret).build();
    }

    @GET
    @Path("writtenInYears")
    @Produces("application/json; charset=UTF-8")
    public Response getBooksFromYearBetween(@Context UriInfo info) {
        int from = Integer.valueOf(info.getQueryParameters().getFirst("from"));
        int to = Integer.valueOf(info.getQueryParameters().getFirst("to"));

        List<BookShortDto> ret = bookDao.getAll().stream().filter(book -> book.getReleaseDate() < to && book.getReleaseDate() > from).map(BookShortDto::new).collect(Collectors.toList());
        return Response.status(200).entity(ret).build();
    }

    @POST
    @Consumes("application/json; charset=UTF-8")
    @Produces("application/json; charset=UTF-8")
    public Response addBook(BookDto aBook) {
        BookEntity ent = bookDao.addBook(BookDtoToEntity.toEntity(aBook));
        BookDto ret = new BookDto(ent);
        return Response.status(201).entity(ret).build();
    }

    @DELETE
    @Path("{id}")
    @Consumes("application/json; charset=UTF-8")
    @Produces("application/json; charset=UTF-8")
    public Response removeBook(@PathParam("id") long aId) {
        bookDao.remove(aId);
        return Response.status(204).build();
    }

    @PUT
    @Consumes("application/json; charset=UTF-8")
    @Produces("application/json; charset=UTF-8")
    public Response editBook(BookDto aBook) {
        bookDao.update(BookDtoToEntity.toEntity(aBook));
        return Response.status(200).entity(aBook).build();
    }
}
