package pl.wizard.software.entities;

import javax.persistence.Embeddable;

@Embeddable
public class WarehouseLocation {

    private Integer x;
    private Integer y;
    private Integer z;
}
