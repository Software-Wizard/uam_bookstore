package pl.wizard.software.entities;

import javax.persistence.*;
import java.util.Set;

@Entity
public class OrderEntity extends AbstractBaseEntity{

    @OneToOne
    private AbstractCustomerEntity customer;
    @ManyToMany
    private Set<BookEntity> books;
    @Enumerated
    private OrderState state;
}
