package pl.wizard.software.entities;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

@Entity
public class WarehouseEntity extends AbstractBaseEntity{

    @OneToOne
    private BookEntity book;
    private Integer amount;

    @Embedded
    private WarehouseLocation location;

}
