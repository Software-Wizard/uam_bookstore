package pl.wizard.software.entities;


import javax.persistence.Entity;
import javax.persistence.OneToOne;

@Entity
public class WaybillEntity extends AbstractBaseEntity{

    @OneToOne
    private AddressEntity adress;
    @OneToOne
    private CourierDriverEntity courier;
}
