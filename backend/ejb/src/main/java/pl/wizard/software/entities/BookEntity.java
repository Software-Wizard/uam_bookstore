package pl.wizard.software.entities;

import javax.batch.api.BatchProperty;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table (name = "BOOK")
public class BookEntity extends AbstractBaseEntity {

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<AuthorEntity> authors;
    private String title;
    private double price;
    private double rating;
    private int releaseDate;

    public BookEntity() {
        this.authors = new HashSet<>();
    }

    public BookEntity(long aId) {
        super(aId);
        this.authors = new HashSet<>();
    }

    public BookEntity(Long id, AuthorEntity author, String title, double price, double rating, int releaseDate) {
        this(id);
        authors.add(author);
        this.title = title;
        this.price = price;
        this.rating = rating;
        this.releaseDate = releaseDate;
    }

    public AuthorEntity getAuthor() {
        return authors.stream().findAny().get();
    }

    public Set<AuthorEntity> getAuthors() {
        return authors;
    }

    public String getTitle() {
        return title;
    }

    public double getPrice() {
        return price;
    }

    public double getRating() {
        return rating;
    }

    public int getReleaseDate() {
        return releaseDate;
    }

    public void addAuthor(AuthorEntity author) {
        this.authors.add(author);
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public void setReleaseDate(int releaseDate) {
        this.releaseDate = releaseDate;
    }

    public void update(BookEntity aBook) {
        authors.add(aBook.getAuthor());
        title = aBook.getTitle();
        price = aBook.getPrice();
        rating = aBook.getRating();
        releaseDate = aBook.getReleaseDate();
    }


}
