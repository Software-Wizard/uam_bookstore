/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.wizard.software;

import pl.wizard.software.entities.AuthorEntity;
import pl.wizard.software.entities.BookEntity;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Stateless
public class BookDao {

    private final Set<BookEntity> books = new HashSet<>();
    @PersistenceContext(unitName = "primary")
    protected EntityManager em;


    public BookDao() {
    }

    public Optional<BookEntity> getBookById(long aId){
        return Optional.of(em.find(BookEntity.class, aId));
    }

    public List<BookEntity> getAll() {
        return em.createQuery("SELECT b FROM BookEntity b join fetch b.authors", BookEntity.class).getResultList();
    }

    public BookEntity addBook(BookEntity aBook) {
        em.persist(aBook);
        return aBook;
    }

    public void remove(long aId) {
        books.remove(new BookEntity(aId));
    }

    public BookEntity update(BookEntity bookEntity) {
        BookEntity dbBook = getBookById(bookEntity.getId()).get();
        dbBook.update(bookEntity);
        BookEntity ret = em.merge(bookEntity);
        return ret;
    }

    public void test() {
        AuthorEntity author = em.find(AuthorEntity.class, 2l);
        author.getBooks().stream().forEach(System.out::println);
    }


    public AuthorEntity test2() {
        AuthorEntity author = em.find(AuthorEntity.class, 2l);
        author.setName("NEW_NAME");
//        em.flush();
        author.setComment("asdf");
        return author;
    }
}
