package pl.wizard.software.entities;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

@Entity
public abstract class AbstractCustomerEntity extends AbstractBaseEntity{

    private Boolean vipStatus;
    @OneToOne
    private AddressEntity address;
}
