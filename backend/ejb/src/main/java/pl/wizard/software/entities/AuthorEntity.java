package pl.wizard.software.entities;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table
public class AuthorEntity extends AbstractBaseEntity{

    private String name;
    private String surname;
    @ManyToMany(mappedBy = "authors")
    private Set<BookEntity> books;

    public AuthorEntity() {
    }

    public AuthorEntity(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Set<BookEntity> getBooks() {
        return books;
    }

    public void setBooks(Set<BookEntity> books) {
        this.books = books;
    }
}
