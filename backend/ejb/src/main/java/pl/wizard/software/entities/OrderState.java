package pl.wizard.software.entities;

public enum OrderState {
    PENDING_PAYMENT, PROCESSING, COMPLETED, DONE, SENT, CANCELLED, REFUNDED
}
