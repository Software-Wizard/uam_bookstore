package pl.wizard.software;

import pl.wizard.software.entities.AuthorEntity;
import pl.wizard.software.entities.BookEntity;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Singleton
@Startup
public class InitializeDatabaseBean {


    @PersistenceContext(unitName = "primary")
    protected EntityManager em;

    @PostConstruct
    public void init(){
        AuthorEntity author1 = new AuthorEntity();
        author1.setName("name1");
        author1.setSurname("surname1");

        AuthorEntity author2 = new AuthorEntity();
        author2.setName("name2");
        author2.setSurname("surname2");

        AuthorEntity author3 = new AuthorEntity();
        author3.setName("name3");
        author3.setSurname("surname3");

        AuthorEntity author4 = new AuthorEntity();
        author4.setName("name4");
        author4.setSurname("surname4");

        BookEntity book1 = new BookEntity();
        BookEntity book2 = new BookEntity();
        book1.setTitle("Test1");
        book1.addAuthor(author1);
        book1.addAuthor(author2);
        book1.addAuthor(author3);
        book1.addAuthor(author4);

        book2.setTitle("Test2");
        book2.addAuthor(author1);

        em.persist(book1);
        em.persist(book2);
    }
}
