package pl.wizard.software.entities;

import javax.persistence.*;
import java.util.Set;

@Entity
public class CourierCompanyEntity extends AbstractBaseEntity{

    private String name;
    @OneToMany(mappedBy = "company")
    private Set<CourierDriverEntity> drivers;
    @OneToOne
    private AddressEntity address;
}
