package pl.wizard.software.entities;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class CourierDriverEntity extends AbstractBaseEntity{

    @ManyToOne
    private CourierCompanyEntity company;
    private String phoneNumber;
    private String name;
    private String surname;
}
