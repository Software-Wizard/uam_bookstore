package pl.wizard.software.entities;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.criteria.Order;

@Entity
public class Invoice extends AbstractBaseEntity{

    private String number;
    @OneToOne
    private AbstractCustomerEntity customer;
    @OneToOne
    private OrderEntity order;
}
