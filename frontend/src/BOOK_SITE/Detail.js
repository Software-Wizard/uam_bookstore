import React, {Component} from 'react';
import "./detail.css"
import BookDetailWidget from "./BookDetailWidget";

class Detail extends Component {

    constructor() {
        super();
        this.state = {
            selectedBook: {}
        };
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.selectedBookId !== this.props.selectedBookId) {
            fetch("http://localhost:8080/war/book/" + nextProps.selectedBookId)
                .then((resp) => resp.json())
                .then(data => {
                    this.setState({
                        selectedBook: data
                    })
                })
        }
    }

    render() {
        return (
            <div className="detailWrapper">
            <BookDetailWidget
                title={this.state.selectedBook["title"]}
                author={this.state.selectedBook["author"]}
                price={this.state.selectedBook["price"]}
            />
            </div>
        )
    };
}

export default Detail;