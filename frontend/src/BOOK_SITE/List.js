import React, {Component} from 'react';
import {Link} from "react-router-dom";
import "./list.css"

class List extends Component {
    constructor() {
        super();
        this.state = {
            allBooks: [],
        }
    }

    componentDidMount() {
        fetch("http://localhost:8080/war/book")
            .then((resp) => resp.json())
            .then(data => {
                this.setState({
                    allBooks: data,
                });
            })
    }


    render() {
        return (
            <div className="listWrapper">
                <ul>
                    {this.state.allBooks.map(objectToRender =>
                        <li>
                            <Link to={"/book/" + objectToRender["id"]}> {objectToRender["title"]} </Link>
                        </li>)}
                </ul>
            </div>
        )
    };


}

export default List;