import React, {Component} from 'react';
import List from "./List";
import Detail from "./Detail";

class BookSite extends Component {


    constructor() {
        super()
        this.state = {
            selectedBookId: 0
        }
    }

    render() {
        return (
            <div>
                <div>
                    <List callback={this.setSelectedBookId}/>
                </div>
                <div>
                    <Detail selectedBookId={this.props.match.params.id}/>
                </div>
            </div>
        )
    };
}

export default BookSite;