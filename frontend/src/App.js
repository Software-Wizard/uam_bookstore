import React, {Component} from 'react';
import './App.css';
import {
    BrowserRouter as Router,
    Route,
    Link
} from 'react-router-dom';
import BookDetailWidget from "./BOOK_SITE/BookDetailWidget";
import Detail from "./BOOK_SITE/Detail";
import BookSite from "./BOOK_SITE/BookSite";

class App extends Component {
    render() {
        return (
            <Router>
                <div className="container">
                    <header>
                        <nav>
                            <div className="logo">
                                LOGO
                            </div>
                            <ul>
                                <li className="active">
                                    <Link to="/">Home</Link>
                                </li>
                                <li>
                                    <Link to="/book/0">Books</Link>
                                </li>
                            </ul>
                        </nav>
                    </header>
                    <main>
                        <Route path="/book/:id" component={BookSite}/>
                    </main>
                    <footer>
                        <p>Przemysław Wieczorek Software-Wizard</p>
                    </footer>
                </div>
            </Router>
        );
    }
}

export default App;
